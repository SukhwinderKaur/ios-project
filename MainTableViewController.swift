//
//  MainTableViewController.swift
//  project
//
//  Created by MacStudent on 2017-12-12.
//  Copyright © 2017 MacStudent. All rights reserved.
//

import UIKit
import CoreData

class MainTableViewController: UIViewController, UITableViewDelegate,UITableViewDataSource
{
    //outlet table view for movie list with picture and review
    @IBOutlet var tableView: UITableView!
    
    //core data
    let appDel=UIApplication.shared.delegate as? AppDelegate
    var myContext:NSManagedObjectContext?
    
    //array of tuples for coredata
    var MovieArrayOfTuples:[(name:String,age:Int16,movieName:String,cinemaName:String,descriptionMovie:String,movieImage:UIImage?,address:String)]=[]
    
    //viewWillAppear: is triggered in response to a change in the state of the application
    override func viewWillAppear(_ animated: Bool) {
    // reloadData() :Reloads the rows and sections of the table view.
        tableView.reloadData()
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //The contentInset sets what the "margins" within the tableview.
        tableView.contentInset=UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0)
        //row height
        tableView.rowHeight=130
        //automatic adustment of row height
        tableView.estimatedRowHeight=UITableViewAutomaticDimension
        //context for core data
        myContext=appDel?.persistentContainer.viewContext
        //performing function call
        myContext?.perform {
            self.queryingDB()
        }
    }
        //returns no of tuples in table view
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return MovieArrayOfTuples.count
    }
    
    //putting data from core data to table view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMovie", for: indexPath) as! MovieCellViewController
        cell.movieName.text=MovieArrayOfTuples[indexPath.row].movieName
        cell.descMovie.text=MovieArrayOfTuples[indexPath.row].descriptionMovie
        cell.movieImage.image = MovieArrayOfTuples[indexPath.row].movieImage
        return cell
    }
    
    //seque from main to movie detail view controller to show all the details if user clicks on a movie cell
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="movieDetails"{
            var desVC = segue.destination
            if let vc = desVC as? UINavigationController {
                desVC = vc.viewControllers[0]
            }
            if let vc = desVC as? MovieDetailsViewController {
                let didSelectedContactToShowDetails = tableView.indexPathForSelectedRow?[1]
                vc.movieInfo = MovieArrayOfTuples[didSelectedContactToShowDetails!]
                vc.selectedContextIndex=didSelectedContactToShowDetails
            }
        }
    }
    
    //querying core data from data model
    func queryingDB(){
        let fetchAllContactRequest:NSFetchRequest<Movie>=Movie.fetchRequest()
        do{
            let fetchedList=try myContext?.fetch(fetchAllContactRequest)
            if !(fetchedList?.isEmpty)!{
                insertFetchedListInTableToDisplay(List: fetchedList!)
            }
            
        }catch{print("Fetch  Error")}
    }
    //insert data in table cell from core data
    func insertFetchedListInTableToDisplay(List:[Movie]){
        
        for movieItem in List{
            let img=UIImage(data:(movieItem.photoOfMovie! as NSData) as Data  )
            MovieArrayOfTuples.append((movieItem.name!,movieItem.age,movieItem.nameOfMovie!,movieItem.nameOfCinema!,movieItem.descriptionOfMovie!, img,movieItem.address!))
            tableView.reloadData()
        }
        try! myContext?.save()
    }
    
}

