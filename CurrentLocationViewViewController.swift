//
//  CurrentLocationViewViewController.swift
//  project
//
//  Created by sukh on 2017-12-17.
//  Copyright © 2017 MacStudent. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class CurrentLocationViewViewController: UIViewController , CLLocationManagerDelegate
{
    //outlet for current location
    @IBOutlet weak var map: MKMapView!
    
    //location manager
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        //requesting user for authorization and start updating location and set show user location to True
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        map.showsUserLocation = true
    }
    
    //function for geeting current location of user using CLLocation
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let lastLocation: CLLocation = locations[locations.count - 1]
        
        animateMap(lastLocation)
    }
    //shows region on map with current user location
    func animateMap(_ location: CLLocation) {
        let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 1000, 1000)
        map.setRegion(region, animated: true)
    }
    
}
