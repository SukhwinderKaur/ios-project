//
//  MovieCellViewController.swift
//  project
//
//  Created by MacStudent on 2017-12-14.
//  Copyright © 2017 MacStudent. All rights reserved.
//

import UIKit

class MovieCellViewController: UITableViewCell {

    //outlets for movie cell
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var descMovie: UILabel!
    @IBOutlet weak var movieName: UILabel!
    
    //awakeFromNib() guarantee to have all its outlet and action connections already established.
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    //Sets the selected state of the cell, optionally animating the transition between states.
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}

