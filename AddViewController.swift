//this class is associated with Add new movie view controller in storyboard
// it shows form for entering details about movie and saves that data in core data model
//it also has a button which goes to current Location view controller & shows the current location of user on map when clicked

import UIKit
import CoreData

class AddViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    //outlets for all text fields
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var age: UITextField!
    
    @IBOutlet weak var address: UITextField!
    
    @IBOutlet weak var cinemaName: UITextField!
    
    @IBOutlet weak var movieName: UITextField!
    
    @IBOutlet weak var descriptionMovie: UITextView!
    
    @IBOutlet weak var movieImage: UIImageView!
    
    //context for persistend data so that we can save data enetered in textFields
    let contx=(UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    
    //image picker default func if user cancels image picking
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //default image picker for adding image to image view which is selected from gallery
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        movieImage.image=img
        dismiss(animated: true, completion: nil)
    }
    
    //action on image tap gesture using image picker function for opening & selecting image from gallery
    @IBAction func choseImageTap(_ sender: Any) {
        let imgPickerController=UIImagePickerController()
        imgPickerController.sourceType = .photoLibrary
        imgPickerController.delegate=self
        present(imgPickerController, animated: true, completion: nil)
        
    }
    
    //adding data from text fields and imageView to core data in Movie Table
    func addMovieToDB() {
        if let myContext=contx{
            //seletcing entity
            let movie=Movie(context: myContext)
            movie.name = name.text!
            movie.age = Int16(age.text!)!
            movie.address = address.text!
            movie.nameOfMovie = movieName.text!
            movie.nameOfCinema = cinemaName.text!
            movie.descriptionOfMovie = descriptionMovie.text!
            let imgData=UIImagePNGRepresentation(movieImage.image!)
            movie.photoOfMovie=imgData as NSData?
            print("Insertion DOne")
            do{
                try myContext.save()
            }catch{
                print("Context Save Error")
            }
            
        }
        
    }
    
    //action on clicking save bar Button , it saves data in Table & resets all the fields
    @IBAction func saveMovie(_ sender: Any) {
        addMovieToDB()
        name.text=""
        age.text=""
        movieName.text=""
        cinemaName.text=""
        descriptionMovie.text=""
        address.text=""
        movieImage.image=UIImage(named : "ImageIcon.jpg")
    }
    
    //hiding view on cancel click
    /*override func viewDidDisappear(_ animated: Bool) {
        dismiss(animated: true, completion: nil)
    }*/
    
    
    @IBAction func CancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
    }
}

