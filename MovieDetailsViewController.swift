//
//  MovieDetailsViewController.swift
//  project
//
//  Created by MacStudent on 2017-12-14.
//  Copyright © 2017 MacStudent. All rights reserved.
//

import UIKit
import MapKit

class MovieDetailsViewController: UIViewController , CLLocationManagerDelegate {
    //outlets for all elements to view on screen for details of selected movie

    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var cinema: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var name: UILabel!
    //map for address entered by user when user adds new movie details
    @IBOutlet weak var addressMap: MKMapView!
    
    //index for selected movie
    var selectedContextIndex:Int?
    
    //tuple for storing all details get from main list of movie
    var movieInfo:(name:String,
                    age:Int16,
                    movieName:String,
                    cinemaName:String,
                    descriptionMovie:String,
                    movieImage:UIImage?,
                    address:String)=("",0,"","","",nil,"")
    override func viewDidLoad() {
        super.viewDidLoad()
        //call function updating view with selected movie details
        updateUI()
    }
    //set labels , image view and map with selected movie details
    func updateUI(){
        self.title = movieInfo.movieName
        name.text="Name is "+movieInfo.name
        age.text="Age is "+String(movieInfo.age)
        cinema.text="Cinema Name is "+movieInfo.cinemaName
        movieDescription.text="Review is "+movieInfo.descriptionMovie
        address.text="Address is "+movieInfo.address
        
        //converting string from address to exact location and mark it on map
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(movieInfo.address) { [weak self] placemarks, error in
            if let placemark = placemarks?.first, let location = placemark.location {
                let mark = MKPlacemark(placemark: placemark)
                
                if var region = self?.addressMap.region {
                    region.center = location.coordinate
                    region.span.longitudeDelta /= 8.0
                    region.span.latitudeDelta /= 8.0
                    self?.addressMap.setRegion(region, animated: true)
                    self?.addressMap.addAnnotation(mark)
                }
            }
        }

        movieImage.image=movieInfo.movieImage
    }
    
}

